import boto.ec2
import time
import random
import string

def launch_machine(user_name):
    conn = boto.ec2.connect_to_region("sa-east-1")
    user_key = user_name+"_key"+"".join( [random.choice(string.digits+string.letters) for i in xrange(4)] )
    key = conn.create_key_pair(user_key)

    #launching the user machine
    reservation = conn.run_instances(
            'ami-2c6d1840', #Ubuntu:ami-34afc458 Template-image:ami-2c6d1840
            key_name=user_key,
            instance_type='t2.micro',
            security_group_ids=['sg-ee5cf989']
           )

    #waiting for the instance to be running (getting instance status)
    instance = reservation.instances[0]

    while instance.state != 'running':
        print '...instance is %s' % instance.state
        time.sleep(10)
        instance.update()

    #getting public DNS of instance
    instance_dns = instance.public_dns_name
    print "DNS: "+str(instance_dns)

    #getting public IP of instance
    instance_ip = instance.ip_address
    print "IP: "+str(instance_ip)

    #saving user key
    key.save('~/Documents/2017.1/')

    key_path = "/Users/vilmabezerraalves/Documents/2017.1/"+user_key+".pem"

    return (instance.id, instance_dns, instance_ip, key_path)
