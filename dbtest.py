import psycopg2

def main():
    cur = connection()
    #insert_req(cur, user_name, user_email, user_phone)
    selectall_req(cur)

def connection():
    try:
        conn = psycopg2.connect("dbname='finalpillow_db' user='vilmabezerra' host='final-pillow.cwehn9dmhplc.sa-east-1.rds.amazonaws.com' password='lindo0607'")
        conn.autocommit = True
        print "Connecting..."
    except:
        print "I am unable to connect to the database"

    cur = conn.cursor()
    return cur

def insert_req(cur, user_name, user_email, user_phone):
    if thereis_req(cur, user_email):
        print "You have already made a requisition. Wait for administrator response!"
    else:
        query = """INSERT INTO requisition(
                       name, email, phone)
                       VALUES (%s, %s, %s);"""

        params = (user_name, user_email, user_phone)

        print cur.mogrify(query, params)

        try:
            cur.execute(query, params)
            print "Requisition added successfully"
            print user_name+" "+user_email+" "+user_phone
            return True
            #selectall_req(cur)
        except psycopg2.Error as e:
            print "I can't INSERT INTO requisition"

    return False

def thereis_req(cur, user_email):
    query = """SELECT email
	           FROM requisition
               WHERE email = %s"""
    try:
        cur.execute(query, (user_email,))
        if cur.rowcount > 0:
            return True
    except psycopg2.Error as e:
        print "I can't SELECT FROM WHERE in requisition"

    return False

def delete_req(cur, user_email):
    query = """DELETE FROM requisition
	           WHERE email = %s;"""
    try:
        cur.execute(query, (user_email,))
    except psycopg2.Error as e:
        print "I can't DELETE FROM requisition"


def insert_user(cur, user_name, user_email, user_phone, password, instance_id, public_dns, public_ip):
    query = """INSERT INTO fp_user(
                   name, email, phone, password, public_dns, public_ip, instance_id)
                   VALUES (%s, %s, %s, %s, %s, %s, %s);"""

    params = (user_name, user_email, user_phone, password, public_dns, public_ip, instance_id)

    try:
        cur.execute(query, params)
        print "User added successfully"
    except psycopg2.Error as e:
        print "I can't INSERT INTO user"
        print e

def selectall_req(cur):
    cur.execute("""SELECT * from requisition""")
    rows = cur.fetchall()
    print "\nRows: \n"
    for row in rows:
        print "   ", row[1], row[2], row[3]

if __name__ == "__main__":
    main()
