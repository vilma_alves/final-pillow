from flask import Flask
from flask import request
from flask import render_template

import dbtest
import launch_instance
import smtplib
import random
import string
import subprocess

app = Flask(__name__, static_url_path='/static')

@app.route('/', methods=['GET', 'POST'])
def requisition():
    if request.method == 'POST':
        do_requisition(request)
        return render_template('bye.html')
    else:
        #return 'Hello World!'
        return render_template('login.html')


def do_requisition(request):
    #get info passed by form with method POST
    user_name = request.form['name']
    user_email = request.form['email']
    user_phone = request.form['phone']

    #connect to amazon DB
    cur = dbtest.connection()
    #insert requisition in DB
    req_inserted = dbtest.insert_req(cur, user_name, user_email, user_phone)

    if req_inserted == True:
        print "Done with Requisition"

        #launch a machine
        instance_id, public_dns, public_ip, key_path = launch_instance.launch_machine(user_name)

        #generate a password
        user_password = generate_password()

        #insert user and her info in database (user table)
        dbtest.insert_user(cur, user_name, user_email, user_phone, user_password,
                           instance_id, public_dns, public_ip)

        #send user an email to let her know her password and that her machine is ready
        send_email(user_email, user_name,user_password, public_dns)

        #delete user from requisition table (She is now a user)
        dbtest.delete_req(cur, user_email)

        #initiaize amazon instance
        init_instance(user_name, user_password, key_path, public_dns)

    return req_inserted

def generate_password():
    password = "".join( [random.choice(string.digits+string.letters) for i in xrange(12)] )
    print password
    return password

def send_email(user_email, user_name, password, public_dns):
    smtp = smtplib.SMTP_SSL('smtp.gmail.com', 465)

    smtp.login('fpillowservice@gmail.com', 'fpservice0607')

    de = 'fpillowservice@gmail.com'
    para = [user_email]
    msg = """From: %s
    To: %s
    Subject: Final Pillow Account

    Hey %s,

    Your Final Pillow Account is all set. Here is info to acess it:
    Your machine's URL: %s
    Your password: %s

    To acess your machine go to hugosc.github.io
    Have Fun!""" % (de, ', '.join(para), user_name, public_dns, password)

    smtp.sendmail(de, para, msg)

    smtp.quit()


def init_instance(uname, passwd, key_path, public_dns):

    endpoint = 'ubuntu@' + public_dns

    chmod_command = ['chmod','600', key_path]

    init_cmd = "node web_shell -u %s -p %s &" % (uname, passwd)

    ssh_command = ['ssh', '-i', key_path, endpoint, init_cmd]

    print (chmod_command)
    print (ssh_command)

    subprocess.call(chmod_command)
    subprocess.call(ssh_command)
